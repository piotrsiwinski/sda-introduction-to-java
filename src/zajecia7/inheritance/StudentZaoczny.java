package zajecia7.inheritance;

import zajecia7.interfaces.Payable;
import zajecia7.interfaces.Workable;

public class StudentZaoczny extends Student implements Workable, Payable{
    public StudentZaoczny(String name, String surname, int age) {
        super(name, surname, age);
    }

    @Override
    public void work() {

    }

    @Override
    public double getSalary() {
        return 0;
    }

    @Override
    public void pay() {

    }
}
